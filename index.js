var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongodb = require('mongodb');
var mongoose = require('mongoose');
var schedule = require('node-schedule');

// Connect to Mongoose
// mongoose.connect('mongodb://root:123456@ds032340.mlab.com:32340/mytravelsdb');
mongoose.connect('mongodb://localhost/product-database');
const dbs = mongoose.connection;

var helpers = require('./lib/helpers');

var routes = require('./routes/home');
var tours = require('./routes/tour');
var user = require('./routes/user');
var client = require('./routes/client');
var cities = require('./routes/city');
var orders = require('./routes/order');
var customers = require('./routes/customer');

Order =require('./models/order');

// Init App
const app = express();

// Create `ExpressHandlebars` instance with a default layout.
var hbs = exphbs.create({
    defaultLayout: 'main',
    helpers: helpers,
});

// View Engine
app.set("views", path.join(__dirname, "views"));
app.engine("handlebars", hbs.engine);
app.set("view engine", "handlebars");

// BodyParser Middleware
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cookieParser());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// Connect Flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});

// Role client
app.use('/', client);

// Role admin
app.use('/admin', routes);
app.use('/admin/tours', tours);
app.use('/admin/user', user);
app.use('/admin/cities', cities);

app.use('/orders', orders);
app.use('/customers', customers);

//Auto delete order at midnight
schedule.scheduleJob('0 0 * * *', () => {
  Order.getOrders((err, orders) => {
    if(err){
      throw err;
    }
    let now = new Date();
    now.setHours(0,0,0,0);
    for (let i = 0; i < orders.length; i++) {
      if(now > orders[i].start_date) {
        Order.removeOrder(orders[i]._id, (err, orders) => {
          if(err){
            throw err;
          }
        });
      }
    }
  });
});

app.set("port", process.env.PORT || 5000);


app.listen(app.get("port"), function() {
  console.log("Server started on port " + app.get("port"));
});
