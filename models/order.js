const mongoose = require('mongoose');

// Order Schema
const orderSchema = mongoose.Schema({
	tourName:{
		type: String,
		required: true
	},
    tourID:{
		type: String,
		required: true
	},
	start_date:{
		type: Date,
		default: Date.now
	},
	name:{
		type: String,
		required: true
	},
    email: {
		type: String,
		required: true
	},
    phone: {
        type: String,
		required: true
    },
});

const Order = module.exports = mongoose.model('Order', orderSchema);

// Get Orders
module.exports.getOrders = (callback, limit) => {
	Order.find(callback).limit(limit);
}

// Get Order
module.exports.getOrderById = (id, callback) => {
	Order.findById(id, callback);
}

// Add Order
module.exports.addOrder = (order, callback) => {
	Order.create(order, callback);
}

// Update Order
module.exports.updateOrder = (id, order, options, callback) => {
	var query = {_id: id};
	var update = {
		tourName: order.tourName,
		tourID: order.tourID,
		name: order.name,
        email: order.email,
        phone: order.phone,
	}
	Order.findOneAndUpdate(query, update, options, callback);
}

// Update Order
module.exports.checkOrder = (order, callback) => {
	var query = {
		tourID: order.tourID,
        email: order.email,
        phone: order.phone,
	}
	Order.find(query, callback);
}

// Update Order
module.exports.searchOrderByTourID = (id, callback) => {
	var query = {
		tourID: id,
	}
	Order.find(query, callback);
}

// Delete Order
module.exports.removeOrder = (id, callback) => {
	var query = {_id: id};
	Order.remove(query, callback);
}
