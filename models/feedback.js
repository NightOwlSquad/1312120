const mongoose = require('mongoose');

// Feedback Schema
const feedbackSchema = mongoose.Schema({
    tourID:{
		type: String,
		required: true
	},
	name:{
		type: String,
		required: true
	},
	email:{
		type: String,
		required: true
	},
    content:{
		type: String,
		required: true
	},
	create_date:{
		type: Date,
		default: Date.now
	}
});

const Feedback = module.exports = mongoose.model('Feedback', feedbackSchema);

// Get Feedbacks
module.exports.getFeedbacks = (callback, limit) => {
	Feedback.find(callback).limit(limit);
}

// Get Feedback
module.exports.getFeedbackById = (id, callback) => {
	Feedback.findById(id, callback);
}

// Get Feedback
module.exports.getFeedbackByTour = (tourID, callback) => {
	Feedback.find({tourID: tourID}, callback);
}

// Add Feedback
module.exports.addFeedback = (feedback, callback) => {
	Feedback.create(feedback, callback);
}

// Update Feedback
module.exports.updateFeedback = (id, feedback, options, callback) => {
	var query = {_id: id};
	var update = {
		name: feedback.name,
		tourID: feedback.tourID,
        email: feedback.email,
		content: feedback.content,
	}
	Feedback.findOneAndUpdate(query, update, options, callback);
}

// Delete Feedback
module.exports.removeFeedback = (id, callback) => {
	var query = {_id: id};
	Feedback.remove(query, callback);
}
