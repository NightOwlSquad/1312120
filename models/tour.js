const mongoose = require('mongoose');

// Tour Schema
const tourSchema = mongoose.Schema({
	image:{
		type: String,
		required: true
	},
	name:{
		type: String,
		required: true
	},
	detail:{
		type: String,
		required: true
	},
	start_city:{
		type: String,
		required: true
	},
	end_city:{
		type: String,
		required: true
	},
	start_date:{
		type: Date,
		default: Date.now
	},
	price:{
		type: Number,
		default: 0
	},
	reduce:{
		type: Number,
		default: 0
	},
	amount:{
		type: Number,
		default: 0
	},
});

const Tour = module.exports = mongoose.model('Tour', tourSchema);

// Get Tours
module.exports.getTours = (callback, limit) => {
	Tour.find(callback).limit(limit);
}

// Get Tour
module.exports.getTourById = (id, callback) => {
	Tour.findById(id, callback);
}

// Add Tour
module.exports.addTour = (tour, callback) => {
	Tour.create(tour, callback);
}

// Update Tour
module.exports.updateTour = (id, tour, options, callback) => {
	var query = {_id: id};
	var update = {
		image: tour.image,
		name: tour.name,
		detail: tour.detail,
		start_city: tour.start_city,
		end_city: tour.end_city,
		start_date: tour.start_date,
		price: tour.price,
		reduce: tour.reduce,
		amount: tour.amount,
	}
	Tour.findOneAndUpdate(query, update, options, callback);
}

// Delete Tour
module.exports.removeTour = (id, callback) => {
	var query = {_id: id};
	Tour.remove(query, callback);
}

// Delete Tour
module.exports.searchTours = (query, callback) => {
	Tour.find(query, callback);
}
