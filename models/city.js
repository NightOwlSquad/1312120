const mongoose = require('mongoose');

// City Schema
const citySchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	location:{
		type: Boolean,
		default: true
	},
	create_date:{
		type: Date,
		default: Date.now
	}
});

const City = module.exports = mongoose.model('City', citySchema);

// Get Cities
module.exports.getCities = (callback, limit) => {
	City.find(callback).limit(limit);
}

// Get City
module.exports.getCityById = (id, callback) => {
	City.findById(id, callback);
}

// Add City
module.exports.addCity = (city, callback) => {
	City.create(city, callback);
}

// Update City
module.exports.updateCity = (id, city, options, callback) => {
	var query = {_id: id};
	var update = {
		name: city.name,
		location: city.location,
	}
	City.findOneAndUpdate(query, update, options, callback);
}

// Delete City
module.exports.removeCity = (id, callback) => {
	var query = {_id: id};
	City.remove(query, callback);
}
