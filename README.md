# Welcome to my repository
- Fullname: Hà Minh Đạt
- MSSV: 1312120
- Email SV: 1312120@student.hcmus.edu.vn

### Git global setup

git config --global user.name "Ha Minh Dat" <br />
git config --global user.email "dathm.dev@gmail.com"

### Create a new repository

git clone git@gitlab.com:NightOwlSquad/1312120.git <br />
cd 1312120 <br />
touch README.md <br />
git add README.md <br />
git commit -m "add README" <br />
git push -u origin master

### Existing folder

cd existing_folder <br />
git init <br />
git remote add origin git@gitlab.com:NightOwlSquad/1312120.git <br />
git add . <br />
git commit -m "Initial commit" <br />
git push -u origin master <br />

### Existing Git repository

cd existing_repo <br />
git remote add origin git@gitlab.com:NightOwlSquad/1312120.git <br />
git push -u origin --all <br />
git push -u origin --tags
