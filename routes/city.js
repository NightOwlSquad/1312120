var express = require('express');
var router = express.Router();

City =require('../models/city');

let Cities = [];

const initData = () => {
    City.getCities((err, cities) => {
		if(err){
			throw err;
		}
		Cities = cities;
	});
}

router.get("/all", function(req, res) {
    City.getCities((err, cities) => {
		if(err){
			throw err;
		}
		Cities = cities;
		res.render("cities", {
			cities: Cities
		});
	});
});

router.get("/add", ensureAuthenticated, function(req, res) {
    res.render("addCity");
});

router.post("/added", ensureAuthenticated, function(req, res) {
	var city = { name:req.body.name };
	if (req.body.location) {
		city.location = true;
	} else {
		city.location = false;
	}
	Cities.push(city);
		City.addCity(city, (err, city) => {
			if(err){
				throw err;
			}
			initData();
			res.redirect("/admin/cities/all");
		});
});

router.get("/remove/:_id", ensureAuthenticated, function(req, res) {
  var id = req.params._id;
  City.removeCity(id, (err, city) => {
		if(err){
			throw err;
		}
        initData();
		res.redirect("/admin/cities/all");
	});
});

router.get("/edit/:_id", ensureAuthenticated, function(req, res) {
  City.getCityById(req.params._id, (err, city) => {
		if(err){
			throw err;
		}
		res.render("editCity", {
			id: city._id,
			name: city.name,
			location: city.location,
		});
	});
});

router.get("/detail/:_id", function(req, res) {
  City.getCityById(req.params._id, (err, city) => {
		if(err){
			throw err;
		}
		res.render("cityDetail", {
			id: city._id,
			name: city.name,
			location: city.location,
		});
	});
});

router.post("/edited/:_id", ensureAuthenticated, function(req, res) {
  var id = req.params._id;
	var city = req.body;
	City.updateCity(id, city, {}, (err, city) => {
		if(err){
			throw err;
		}
        initData();
		res.redirect("/admin/cities/detail/"+id);
	});
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','You need to login to add, delete, edit cities');
		res.redirect('/admin/user/login');
	}
}

module.exports = router;
