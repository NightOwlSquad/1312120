const express = require('express');
const router = express.Router();

City =require('../models/city');
Tour =require('../models/tour');
Order =require('../models/order');
Feedback =require('../models/feedback');

let Tours = [];
let Cities = [];
let Feedbacks = [];

router.get("/", function(req, res) {
  Tour.getTours((err, tours) => {
    if(err){
			throw err;
		}
		Tours = tours;
    Tours.reverse();
    let minTours = [];
    for (let i = 0; i < 8; i++){
      minTours.push(Tours[i]);
    }
    City.getCities((err, cities) => {
      if(err){
        throw err;
      }
      Cities = cities.map((city)=> city.name);
      if (minTours.length === 0)
          res.render("searchTours", {layout: 'client', tours: minTours, cities: Cities, searchValue: searchValue});
      for(let i = 0; i < minTours.length; i++) {
        Order.searchOrderByTourID(minTours[i]._id, (err, orders) => {
          if(err){
            throw err;
          }
          minTours[i].slot = orders.length;
          if (i === minTours.length - 1)
            res.render("top", {layout: 'client', tours: minTours, cities: Cities});
        });
      }
    });
  });
});

router.get("/search", function(req, res) {
  let searchValue = req.body;

  Tour.getTours((err, tours) => {
    if(err){
			throw err;
		}
		Tours = tours;
    City.getCities((err, cities) => {
      if(err){
        throw err;
      }
      Cities = cities.map((city)=> city.name);
      if (tours.length === 0)
          res.render("searchTours", {layout: 'client', tours: tours, cities: Cities, searchValue: searchValue});
      for(let i = 0; i < tours.length; i++) {
        Order.searchOrderByTourID(tours[i]._id, (err, orders) => {
          if(err){
            throw err;
          }
          tours[i].slot = orders.length;
          if (i === tours.length - 1)
            res.render("searchTours", {layout: 'client', tours: tours.reverse(), cities: Cities, searchValue: searchValue});
        });
      }
    });
  });
});

router.post("/search", function(req, res) {
  let searchValue = req.body;
  let query = {};
  if (searchValue.start_city !== "1")
    query.start_city = searchValue.start_city;
  if (searchValue.end_city !== "1")
    query.end_city = searchValue.end_city;
  if (searchValue.start_date)
    query.start_date = searchValue.start_date;

  if (searchValue.priceRange === "2")
    query.price = {$gt: 999999, $lt: 2000000}
  if (searchValue.priceRange === "3")
    query.price = {$gt: 1999999, $lt: 3000000}
  if (searchValue.priceRange === "4")
    query.price = {$gt: 2999999, $lt: 4000000}
  if (searchValue.priceRange === "5")
    query.price = {$gt: 3999999, $lt: 5000000}
  if (searchValue.priceRange === "6")
    query.price = {$gt: 4999999}

  if(searchValue.reduce !== "0")
    query.reduce = Number(searchValue.reduce);

  Tour.getTours((err, tours) => {
    if(err){
			throw err;
		}
    Tours = tours;
    Tour.searchTours(query, (err, tours) => {
      if(err){
        throw err;
      }
      for(let i = 0; i < tours.length; i++) {
        Order.searchOrderByTourID(tours[i]._id, (err, orders) => {
          if(err){
            throw err;
          }
          tours[i].length = orders.length;
        });
      }
      City.getCities((err, cities) => {
        if(err){
          throw err;
        }
        Cities = cities.map((city)=> city.name);
        let filter =[];
        if (tours.length === 0)
          res.render("searchTours", {layout: 'client', tours: tours, cities: Cities, searchValue: searchValue});
        for(let i = 0; i < tours.length; i++) {
          Order.searchOrderByTourID(tours[i]._id, (err, orders) => {
            if(err){
              throw err;
            }
            tours[i].slot = orders.length;
            if(searchValue.checkAmount === "1")
            {
              filter.push(tours[i]);
            }
            if(searchValue.checkAmount === "2" && orders.length < tours[i].amount)
            {
              filter.push(tours[i]);
            }
            if(searchValue.checkAmount === "3" && orders.length >= tours[i].amount)
            {
              filter.push(tours[i]);
            }
            if (i === tours.length - 1)
              res.render("searchTours", {layout: 'client', tours: filter.reverse(), cities: Cities, searchValue: searchValue, });
          });
        }
      });
    });
  });
});

router.get("/feedback/:_id", function(req, res) {
   Feedback.getFeedbackByTour(req.params._id, (err, feedback) => {
     if(err){
        throw err;
      }
      res.json(feedback);
   })
});

router.get("/tourdetail/:_id", function(req, res) {
  Tour.getTourById(req.params._id, (err, tour) => {
		if(err){
			throw err;
		}
    Feedback.getFeedbackByTour(req.params._id, (err, feedback) => {
      if(err){
        throw err;
      }
      let now = new Date();
      now.setHours(0,0,0,0);
      Feedbacks = feedback;
      Feedbacks.reverse();
      let object = {
        layout: 'client',
        tour: tour,
        flat: true,
        feedbacks: Feedbacks,
        length: Feedbacks.length,
      };

      // for (let i = 0; i < object; i++) {
      //   object.feedbacks.create_date = object.feedbacks.create_date.toString();
      // }

      if(now > tour.start_date) {
        object.flat = false;
      }
      Order.searchOrderByTourID(tour._id, (err, orders) => {
        if(err){
          throw err;
        }
        if(orders.length >= tour.amount) {
          object.flat = false;
        }
        object.tour.slot = orders.length;
        res.render("tourDetailClient", object);
      });
    });
	});
});

router.post("/search", function(req, res) {
  let searchValue = req.body;
  let query = {};
  if (searchValue.start_city !== "---Start place---" && searchValue.start_city !== "1")
      query.start_city = searchValue.start_city;
  if (searchValue.end_city !== "---Destination---" && searchValue.end_city !== "1")
      query.end_city = searchValue.end_city;
  if (searchValue.start_date)
      query.start_date = searchValue.start_date;

  Tour.getTours((err, tours) => {
    if(err){
			throw err;
		}
		Tours = tours;

    Tour.searchTours(query, (err, tours) => {
      if(err){
        throw err;
      }
      City.getCities((err, cities) => {
        if(err){
          throw err;
        }
        Cities = cities.map((city)=> city.name);
        res.render("searchTours", {layout: 'client', tours: tours, cities: Cities, searchValue: searchValue});
      });
    });
  });
});

router.post("/feedback", function(req, res) {
  const feedback = req.body;
  Feedback.addFeedback(feedback, (err, tour) => {
		if(err){
			throw err;
		}
    Feedbacks.push(req.body);
    res.redirect("/tourdetail/"+feedback.tourID);
	});
});

router.post("/searchname", function(req, res) {
  const searchValue = req.body;
  Tour.getTours((err, tours) => {
    if(err){
			throw err;
		}
    let sTours = [];
    for (let i = 0; i < tours.length; i++) {
      if (ApproximatString(tours[i].name, searchValue.searchValue))
        sTours.push(tours[i]);
    }
    City.getCities((err, cities) => {
      if(err){
        throw err;
      }
      Cities = cities.map((city)=> city.name);
      if (sTours.length === 0)
        res.render("searchTours", {layout: 'client', tours: sTours, cities: Cities, searchValue: searchValue});
      for(let i = 0; i < sTours.length; i++) {
        Order.searchOrderByTourID(sTours[i]._id, (err, orders) => {
          if(err){
            throw err;
          }
          sTours[i].slot = orders.length;
          if (i === sTours.length - 1)
            res.render("searchTours", {layout: 'client', tours: sTours, cities: Cities, searchValue: searchValue});
        });
      }
    });
  });
});

const ApproximatString = (strName, strInput) => {
  const numErr = parseInt(strInput.length * 0.5);
  if (strName.length < (strInput.length - numErr) || strName.length > (strInput.length + numErr))
      return false;
  let i = 0, j = 0, err = 0;
  while (i < strInput.length && j < strName.length ) {
    if (strInput[i] != strName[j])
    {
        err++;
        for (let k = 1; k <= numErr; k++)
        {
            if ((i + k < strInput.length) && strInput[i + k] == strName[j])
            {
                i += k;
                err += k - 1;
                break;
            }
            else if ((j + k < strName.length) && strInput[i] == strName[j + k])
            {
                j += k;
                err += k - 1;
                break;
            }
        }
    }
    i++;
    j++;
  }

  err += strInput.length - i + strName.length - j;
  if (err <= numErr)
      return true;
  else return false;
}

module.exports = router;
