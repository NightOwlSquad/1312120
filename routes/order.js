var express = require('express');
var router = express.Router();

Order =require('../models/order');

let Orders = [];

router.get("/all", function(req, res) {
    Order.getOrders((err, orders) => {
		if(err){
			throw err;
		}
		Orders = orders;
        res.render("orders", {
            orders: Orders
        });
    });
});

router.post("/added", function(req, res) {
	let order = req.body;
	let tourID = order.tourID;
	Order.checkOrder(order, (err, _order) => {
		if(err){
            throw err;
        }
		if(_order.length !== 0) {
			req.flash('error_msg', 'You have booked this order');
			res.redirect("/tourdetail/"+tourID);
			return res.send();
		} else {
			Orders.push(order);
			Order.addOrder(order, (err, _order) => {
				if(err){
					throw err;
				}
				req.flash('success_msg', 'You are booked success');
				res.redirect("/tourdetail/"+tourID);
			});
		}
	});
});

router.get("/remove/:_id", ensureAuthenticated, function(req, res) {
  var id = req.params._id;
  Order.removeOrder(id, (err, order) => {
		if(err){
			throw err;
		}
        initData();
		res.redirect("/admin/orders/all");
	});
});

router.get("/edit/:_id", ensureAuthenticated, function(req, res) {
  Order.getOrderById(req.params._id, (err, order) => {
		if(err){
			throw err;
		}
		res.render("editOrder", {
			id: order._id,
			name: order.name,
			location: order.location,
		});
	});
});

router.get("/detail/:_id", function(req, res) {
  Order.getOrderById(req.params._id, (err, order) => {
		if(err){
			throw err;
		}
		res.render("orderDetail", {
			order: order,
		});
	});
});

router.post("/edited/:_id", ensureAuthenticated, function(req, res) {
  var id = req.params._id;
	var order = req.body;
	Order.updateOrder(id, order, {}, (err, order) => {
		if(err){
			throw err;
		}
        initData();
		res.redirect("/admin/orders/detail/"+id);
	});
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','You need to login to add, delete, edit orders');
		res.redirect('/admin/user/login');
	}
}

module.exports = router;
