var express = require('express');
var router = express.Router();

City =require('../models/city');
Tour =require('../models/tour');

let Tours = [];
let Cities = [];

const initData = () => {
    City.getCities((err, cities) => {
		if(err){
			throw err;
		}
		Cities = cities.map((city)=> city.name);
	});

    Tour.getTours((err, tours) => {
		if(err){
			throw err;
		}
		Tours = tours;
	});
}

router.get("/all", function(req, res) {
    Tour.getTours((err, tours) => {
		if(err){
			throw err;
		}
		Tours = tours;
		res.render("tours", {
			tours: Tours
		});
	});
});

router.get("/add", ensureAuthenticated, function(req, res) {
	City.getCities((err, cities) => {
		if(err){
			throw err;
		}
		Cities = cities.map((city)=> city.name);
		res.render("addTour", {
			cities: Cities
		});
	});
});

router.post("/added", ensureAuthenticated, function(req, res) {
  var tour = req.body;
  tour.price = Number(tour.price);
  tour.reduce = Number(tour.reduce);
  tour.amount = Number(tour.amount);
  Tours.push(tour);
	Tour.addTour(tour, (err, tour) => {
		if(err){
			throw err;
		}
		initData();
        res.redirect("/admin/tours/all");
	});
});

router.get("/remove/:_id", ensureAuthenticated, function(req, res) {
  var id = req.params._id;
  Tour.removeTour(id, (err, tour) => {
		if(err){
			throw err;
		}
        initData();
		res.redirect("/admin/tours/all");
	});
});

router.get("/edit/:_id", ensureAuthenticated, function(req, res) {
  Tour.getTourById(req.params._id, (err, tour) => {
		if(err){
			throw err;
		}
		const date = tour.start_date;
		let day = ("0" + date.getDate()).slice(-2);
		let month = ("0" + (date.getMonth() + 1)).slice(-2);

		let format = date.getFullYear()+"-"+(month)+"-"+(day);
		res.render("editTour", {
			id: tour._id,
			image: tour.image,
			name: tour.name,
			detail: tour.detail,
			start_city: tour.start_city,
			end_city: tour.end_city,
			start_date: format,
			price: Number(tour.price),
			reduce: Number(tour.reduce),
			amount: Number(tour.amount),
			cities: Cities,
    	});
	});
});

router.get("/detail/:_id", function(req, res) {
  Tour.getTourById(req.params._id, (err, tour) => {
		if(err){
			throw err;
		}
		res.render("tourDetail", {
			id: tour._id,
			image: tour.image,
			name: tour.name,
			detail: tour.detail,
			start_city: tour.start_city,
			end_city: tour.end_city,
			start_date: tour.start_date,
			price: tour.price,
			reduce: tour.reduce,
			amount: tour.amount,
		});
	});
});

router.post("/edited/:_id", ensureAuthenticated, function(req, res) {
  var id = req.params._id;
	var tour = req.body;
	Tour.updateTour(id, tour, {}, (err, tour) => {
		if(err){
			throw err;
		}
        initData();
		res.redirect("/admin/tours/detail/"+id);
	});
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','You need to login to add, delete, edit tours');
		res.redirect('/admin/user/login');
	}
}

module.exports = router;
